import express from 'express';
import bodyParser from 'body-parser'
import path from 'path'
import morgan from 'morgan';
import cors  from 'cors';
import  dotenv from 'dotenv';

import apirouter from './api/routes/router';
import {MongoConnect , RedisConnect } from './drivers';

dotenv.config({ path :  path.resolve(__dirname, '.env')});                           
const app: express.Application =  express();  
app.use(cors({ origin: '*' }))
app.disable('x-powered-by')
app.use(bodyParser.urlencoded({ extended: false  , limit : '200 kb'}))
app.use(bodyParser.json({limit : '200 kb'}))
app.use(morgan('dev'))
app.use('/api/',apirouter)

const startserver = async()=>{
      try {
          await MongoConnect();
          await RedisConnect();
          const port = process.env.APP_PORT || 4500;
          app.listen(port, () => console.log( `server listening on port : ${port}`));
        }
      catch (error){
        console.error('\x1b[31m',error);
        process.exit(0)
      }
}
startserver()
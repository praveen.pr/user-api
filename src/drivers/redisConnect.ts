import ioredis  from 'ioredis'
import bluebird from 'bluebird'
const redis = bluebird.promisifyAll(ioredis);

export const RedisConnect = async () =>
{
    global['redisCache'] = new redis({
        port: +process.env.REDIS_PORT, // Redis port
        host: process.env.REDIS_URL, // Redis host
        family: 4, // 4 (IPv4) or 6 (IPv6)
        db: 0,
    });
    console.log('Redis connected');
}
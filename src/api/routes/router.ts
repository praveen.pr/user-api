import { Request, Response, Router } from 'express';
import  authentication  from './authentication';
import {AuthenticationController} from '../controllers/Authentication/authentication';
import {UserController} from '../controllers/Users/users';
import {JWTService} from '../services/jwtService';
const authController = new AuthenticationController();
const userController = new UserController();
const jwtService = new JWTService();

class Api {
  apiRouter: any;
  constructor() {
    this.apiRouter = Router();
    this.initate();
  }
  private initate() {
    this.apiRouter.get('/ping', (req: Request, res: Response) => res.jsonp({ status: "Up From project Router" }))
    this.apiRouter.post('/auth/register', (req: Request, res: Response) => authController.register(req, res));
    this.apiRouter.post('/auth/login', (req: Request, res: Response) => authController.login(req, res));
    this.apiRouter.get('/user/list',(req: Request, res: Response) => userController.list(req, res));
  }
}


export default new Api().apiRouter
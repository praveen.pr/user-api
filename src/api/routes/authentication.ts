import { Request, Response, Router } from 'express';
import {AuthenticationController} from '../controllers/Authentication/authentication';

const authController = new AuthenticationController();
class Authentication {
  authRouter: any;
  constructor() {
    this.authRouter = Router()
    this.initate()
  }
  private initate() {
    this.authRouter.get('/ping', (req: Request, res: Response) => res.jsonp({ status: "Up From project Router" }))
    this.authRouter.get('/test', (req: Request, res: Response) => authController.register(req, res)); 
  }
}


export default new Authentication().authRouter;
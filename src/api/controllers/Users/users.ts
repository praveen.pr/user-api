import {UserService} from '../../services/userService';


export class UserController extends UserService{
    constructor() {
         super()
    }
    list = async(req: any, res: any)=>{
        try{
            const users = await this.find();
            return res.json({
                status: true,
                users,
           });
        }catch(err){
            return res.json({
                status: false,
                message: err.message,
           });
        }
    }

}
import {UserService} from '../../services/userService';


export class AuthenticationController extends UserService{
    constructor() {
         super()
    }

    register = async(req : any, res: any)=>{
           try{
               const {body: {email, password,name }} = req;
               console.log(email,name,password)
               if (!email || !password || !name) {
                    return res.status(422).json({
                         success: false,
                         message: 'email and password required!'
                     });
               }
                const users = await this.createUser({name ,email , password});
                if(!users){
                    return res.json({
                         status: false,
                         message: 'User already exists',
                    });
                }
                return res.json({
                     status: true,
                     users,
                });
           } catch(err){
                return res.json({
                    status: false,
                    message: err.message,
               });
           }
       };

     login =  async(req : any, res: any)=>{
          try {
              const {body: {email, password}} = req;
              if (!email || !password) return res.status(422).json({
                  success: false,
                  message: 'Email and Password are required!'
              });
              const userDetails = await this.loginUser(email , password);
      
              return res.json({
                  success: true,
                  message: 'Login successfully.',
                  data: {userDetails}
              });
          } catch (err) {
               return res.json({
                    status: false,
                    message: err.message,
               });
          }
      }
}
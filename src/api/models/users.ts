import * as mongoose from 'mongoose';
const usersSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type : String,
        required: true
    },
    loginStatus: {
        type: Number,
        default:0,
        required: false
    },
});
const UserModel = mongoose.model('user', usersSchema);
export default UserModel;


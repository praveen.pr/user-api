
import * as jwt from 'jsonwebtoken';

export class JWTService{
    generateToken =(id: string, email: string) => {
            return jwt.sign({id, email}, process.env.JWT_KEY, {expiresIn: process.env.JWT_EXPIRES});
        };

        verifyAccessToken = (token: string) => new Promise((resolve, reject) => {
                jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
                    if (err) {
                        return reject(false);
                    }
                    return resolve(decoded);
                });
            });
}

import UserModel  from '../models/users';
import {encrypt , decrypt} from './encryptDecryptService';
import {RedisService} from './redisServices'
import {JWTService} from './jwtService';
export class UserService{
    redisService;
    jwtService;
    constructor(){
        this.redisService = new RedisService();
        this.jwtService = new JWTService();
    }
   
    async find(): Promise<any> {
            const response = [];
            const user = await UserModel.find({ loginStatus:1 }).exec();
            for(let i=0;i<user.length;i++) {
                const el = user[i];
                const token = this.redisService.getUserToken(el.id);
                response.push({...el , token});
            }
            return response;
    }

    async createUser(userObj: any): Promise<any> {
        const ifExists = await this.findUserEmail(userObj.email);
        console.log(ifExists)
        if(ifExists){throw new Error('user already exists');}
        userObj.password = encrypt(userObj.password);
        return UserModel.create(userObj);
    }

    findUserEmail(email: string): any{
        return UserModel.findOne({email}).exec();
    }

    async loginUser(email: string, password: string):Promise<any>{
            const userDetails = await this.findUserEmail(email);
            if (!userDetails || !userDetails.password) {
                throw new Error('no user exists')
            }
            if (decrypt(userDetails.password) !== password.toString()) {
                throw new Error('invalid credentials')
            }
            const token = await this.jwtService.generateToken(userDetails.id, userDetails.email);
    
            this.redisService.setUserToken(userDetails.id, {
                      token,
                      expiresIn: new Date(new Date().setMilliseconds(+process.env.JWT_EXPIRES))
            });
            userDetails.loginStatus =1;
            await this.updateUser(userDetails.id,userDetails );
            return {userDetails , ...{token} }
    }

    updateUser(id: any , data:any){
        return UserModel.update(id,data);
    }
}
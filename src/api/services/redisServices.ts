export class RedisService{


     setUserToken = (userId, payload) => {
        return  global['redisCache'].set(userId, JSON.stringify(payload));
    }
    
    getUserToken = (userId) => {
        return new Promise((resolve, reject) => {
            global['redisCache'].get(userId, function (err, value) {
                if (err) return reject(err);
                if (value) return resolve(JSON.parse(value));
                return reject();
            });
        });
    }
}